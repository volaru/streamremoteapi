const express = require('express')
const morgan = require('morgan')
const mysql = require('mysql')
const bodyParser = require("body-parser");
const fs = require("fs");


const app = express()
const port = 5050
const numberOfButtonsAlowed = 1000; // High value set for dev/test - Normal value should be 11 (4x3 grid - 11 custom buttons and 1x default button)

// makes the folder public. To maybe use it to share icons? :D
app.use(express.static('./PublicFiles'))

app.use(morgan('short'))

// using this so i can pase the request HTTP request body
app.use(bodyParser.json());


// Reads default button details before starting the server
var fixedbuttons = fs.readFileSync("defaultButtonSettings.json");
var defaultButtons = JSON.parse(fixedbuttons);

// Checks out the list of buttons, if it exists or not
var userButtons;
var contents = fs.existsSync("buttondetails.json");

if (contents) {
    // Read the file
    console.log('loading words');
    var contents = fs.readFileSync("buttondetails.json");
    // Parse it  back to object
    var userButtons = JSON.parse(contents);

} else {
    // Otherwise start with blank list
    console.log('No buttons added');
    userButtons = {};
}

// Starts listening on port 5050
app.listen(port, () => console.log(`Example app listening on port ${port}!`))

// ================== ROUTES ===================

// Root path
app.get('/', (req, res) => res.send('REST API ROOTH PATH TEST!!!!'))

// Sends all button details
app.get('/1/butttondetails', GetButtonList)

// Add a new Button
app.post('/1/addbutton', AddNewButton)

// I pass actual button position value in the Array as JSON body of GET request. Example body: {"buttonPosition":0}
app.get('/1/retrievebutton', GetButtonDetails)

// Update button from app
app.post('/1/updatebutton/:position', UpdateButton)




// ================= FUNCTIONS =================


function GetButtonList(request, response){

    // copy button details JSON to temp variable.
    var temp = JSON.parse(JSON.stringify(userButtons));

    //updating buttondetails before passing them to the user
    temp['ButtonList'].push(defaultButtons.buttonList[0]);
    Str_txt = JSON.stringify(temp, null, 2);
    //console.log(Str_txt)

    response.send(temp);
}

function AddNewButton(request, response){

    //check userButtons array length.
    var limit = userButtons.ButtonList.length;

    if(limit < numberOfButtonsAlowed) {
        //adds the new button to the end of the JSON
        userButtons['ButtonList'].push(request.body);
        Str_txt = JSON.stringify(userButtons, null, 2);
        //console.log(Str_txt)



        fs.writeFile('buttondetails.json', Str_txt, 'utf8', finished);
        function finished(err) {
            console.log('Finished writing buttondetails.json');
            // Don't send anything back until everything is done
            response.send("Button added")
        }
    }
    else {
        response.status(400);
        response.send("Max Buttons reached")
    }


}

function GetButtonDetails(request, response){

    response.send(userButtons.ButtonList[Number(request.body.position)]);

}


function UpdateButton(request, response){

    var position = Number(request.params['position']);
    var newValues = request.body

    if(position <= userButtons.ButtonList.length){
        userButtons.ButtonList[position] = newValues;
        Str_txt = JSON.stringify(userButtons, null, 2);
        //console.log(Str_txt)

        fs.writeFile('buttondetails.json', Str_txt, 'utf8', finished);
        function finished(err) {
            console.log('Finished writing buttondetails.json');
            // Don't send anything back until everything is done
            response.send("Button values updated")
        }
    }
    else {
        response.status(400);
        response.send("Invalid position");
    }












}
